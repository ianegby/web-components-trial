class InventoryItem extends HTMLElement {
    constructor() {
        super();

        const shadow = this.attachShadow({mode: 'open'});
        
        const styles = document.createElement('link');
        styles.setAttribute('rel', 'stylesheet');
        styles.setAttribute('href', 'trial.css');
        shadow.appendChild(styles);

        // Create a root div that conventiaonally has the same name as the element type
        // Good for targetting *all* contained elements using CSS
        const div = document.createElement('div');
        div.classList.add('inventory-item');
        shadow.appendChild(div);

        const title = document.createElement('h2');
        title.classList.add('title');
        title.textContent = this.hasAttribute('data-title') ? this.getAttribute('data-title') : "<title unknown>";
        div.appendChild(title);

        const author = document.createElement('p');
        author.classList.add('author');
        author.textContent = this.hasAttribute('data-author') ? this.getAttribute('data-author') : "<author unknown>";
        div.appendChild(author);
    }
}

customElements.define('inventory-item', InventoryItem);
